
#include "sobel.h"
#include <arm_neon.h>

using namespace std;
using namespace cv;

#define SS 3 // sobel size in both dimensions is 3

const int sobel_kernel_x[3][3] = {
	{1, 0,-1},
	{2, 0,-2},
	{1, 0,-1}};

const int sobel_kernel_y[3][3] = {
	{ 1, 2, 1},
	{ 0, 0, 0},
	{-1,-2,-1}};

void sobel(const Mat& src, Mat& dst)
{
	int i, j, k, l;
	int16_t tempx,tempy;	// 8 bits is too small, also some operations give negatives?

	// multiply by a block and sum - like convolution
	// iterate through the src matrix
	for (i = 0; i < src.rows; i++) {
	for (j = 0; j < src.cols; j++) {
		// zero the matrix value each entry
		tempx = 0;
		tempy = 0;
		// for each of these, multiply by sobel
		// iterate in the main matrix like this, calculating for i,j
		// (i-1,j-1), (i+0,j-1), (i+1,j-1)
		// (i-1,j+0), (i+0,j+0), (i+1,j+0)
		// (i-1,j+1), (i+0,j+1), (i+1,j+1)
		// some conditions won't work, if i is 0, can't evaluate k at 0
		// otherwise similar to going through the sobel matrices
		for (k = 0; k < 3; k++){
			if ((i == 0) && (k == 0))
				continue;
			for (l = 0; l < 3; l++){
				if ((j == 0) && (l == 0))
					continue;
				tempx += (src.at<uint8_t>((i+k-1),(j+l-1))*sobel_kernel_x[k][l]);
				tempy += (src.at<uint8_t>((i+k-1),(j+l-1))*sobel_kernel_y[k][l]);
				// or something
		} }

		dst.at<uint8_t>(i,j) = sqrt((tempy*tempy)+(tempx*tempx));
	} }

	return;
}


void sobel_unroll(const Mat& src, Mat& dst)
{
	int i, j, k, l;
	int16_t tempx,tempy;	// 8 bits is too small, also some operations give negatives?

	const uint8_t* src_ptr = src.ptr<uint8_t>();	
	uint8_t* dst_ptr = dst.ptr<uint8_t>();
	const int rlen = src.cols;
	
	// multiply by a block and sum - like convolution
	// iterate through the src matrix
	// instead of iterating through the sobel matrix, unroll it

	// first handle i = 0, j = 0 case
	i = 0;
	j = 0;
	tempx = ( //sobel_kernel_x[1][1]*src_ptr[i*rlen+j] //src.at<uint8_t>((0),(0))
		  sobel_kernel_x[1][2]*src_ptr[rlen] //src.at<uint8_t>((1),(0))
	        //+ sobel_kernel_x[2][1]*src_ptr[i*rlen+j] //src.at<uint8_t>((0),(1))
		+ sobel_kernel_x[2][2]*src_ptr[rlen+1] ); //src.at<uint8_t>((1),(1))

	tempy = ( //sobel_kernel_y[1][1]*src_ptr[i*rlen+j] //src.at<uint8_t>((0),(0))
		//+ sobel_kernel_y[1][2]*src_ptr[i*rlen+j] //src.at<uint8_t>((1),(0))
	          sobel_kernel_y[2][1]*src_ptr[1] //src.at<uint8_t>((0),(1))
		+ sobel_kernel_y[2][2]*src_ptr[rlen+1] ); //src.at<uint8_t>((1),(1))

		dst_ptr[i*rlen+j] = (uint8_t) sqrt((tempy*tempy)+(tempx*tempx));
		//dst.at<uint8_t>(i,j) = sqrt((tempy*tempy)+(tempx*tempx));

	// handle all i = 0 cases
	i = 0;
	for (j = 1; j < src.cols; j++) {
	tempx = //( sobel_kernel_x[0][1]*src_ptr[i*rlen+j-1] //src.at<uint8_t>((i),(j-1))
		( sobel_kernel_x[0][2]*src_ptr[(i+1)*rlen+j-1] //src.at<uint8_t>((i+1),(j-1))
 	        //+ sobel_kernel_x[1][1]*src_ptr[i*rlen+j] //src.at<uint8_t>((i),(j))
		+ sobel_kernel_x[1][2]*src_ptr[(i+1)*rlen+j] //src.at<uint8_t>((i+1),(j))
	        //+ sobel_kernel_x[2][1]*src_ptr[i*rlen+j+1] //src.at<uint8_t>((i),(j+1))
		+ sobel_kernel_x[2][2]*src_ptr[(i+1)*rlen+j+1] ); //src.at<uint8_t>((i+1),(j+1))

	tempy = ( sobel_kernel_y[0][1]*src_ptr[i*rlen+j-1] //src.at<uint8_t>((i),(j-1))
		+ sobel_kernel_y[0][2]*src_ptr[(i+1)*rlen+j-1] //src.at<uint8_t>((i+1),(j-1))
 	        //+ sobel_kernel_y[1][1]*src_ptr[i*rlen+j] //src.at<uint8_t>((i),(j))
		//+ sobel_kernel_y[1][2]*src_ptr[(i+1)*rlen+j] //src.at<uint8_t>((i+1),(j))
	        + sobel_kernel_y[2][1]*src_ptr[i*rlen+j+1] //src.at<uint8_t>((i),(j+1))
		+ sobel_kernel_y[2][2]*src_ptr[(i+1)*rlen+j+1] ); //src.at<uint8_t>((i+1),(j+1))

		dst_ptr[i*rlen+j] = (uint8_t) sqrt((tempy*tempy)+(tempx*tempx));
		//dst.at<uint8_t>(i,j) = sqrt((tempy*tempy)+(tempx*tempx));
	}

	// handle j = 0 cases
	j = 0;
	for (i = 1; i < src.rows; i++) {
	tempx = ( sobel_kernel_x[1][0]*src_ptr[(i-1)*rlen+j] //src.at<uint8_t>((i-1),(j)) 
		//+ sobel_kernel_x[1][1]*src_ptr[(i)*rlen+j] //src.at<uint8_t>((i),(j))
		+ sobel_kernel_x[1][2]*src_ptr[(i+1)*rlen+j] //src.at<uint8_t>((i+1),(j))
	        + sobel_kernel_x[2][0]*src_ptr[(i-1)*rlen+j+1] //src.at<uint8_t>((i-1),(j+1)) 
		//+ sobel_kernel_x[2][1]*src_ptr[(i)*rlen+j+1] //src.at<uint8_t>((i),(j+1))
		+ sobel_kernel_x[2][2]*src_ptr[(i+1)*rlen+j+1] ); //src.at<uint8_t>((i+1),(j+1))

	tempy = //( sobel_kernel_y[1][0]*src_ptr[(i-1)*rlen+j] //src.at<uint8_t>((i-1),(j)) 
		//+ sobel_kernel_y[1][1]*src_ptr[(i)*rlen+j] //src.at<uint8_t>((i),(j))
		//+ sobel_kernel_y[1][2]*src_ptr[(i+1)*rlen+j] //src.at<uint8_t>((i+1),(j))
	        ( sobel_kernel_y[2][0]*src_ptr[(i-1)*rlen+j+1] //src.at<uint8_t>((i-1),(j+1)) 
		+ sobel_kernel_y[2][1]*src_ptr[(i)*rlen+j+1] //src.at<uint8_t>((i),(j+1))
		+ sobel_kernel_y[2][2]*src_ptr[(i+1)*rlen+j+1] ); //src.at<uint8_t>((i+1),(j+1))

		dst_ptr[i*rlen+j] = (uint8_t) sqrt((tempy*tempy)+(tempx*tempx));
		//dst.at<uint8_t>(i,j) = sqrt((tempy*tempy)+(tempx*tempx));

		for (j = 1; j < src.cols; j++) {
		// don't even need to zero this anymore, just x = x1 + x2 + x3 etc

	tempx = ( sobel_kernel_x[0][0]*src_ptr[(i-1)*rlen+j-1] //src.at<uint8_t>((i-1),(j-1)) //
		//+ sobel_kernel_x[0][1]*src_ptr[(i)*rlen+j-1] //src.at<uint8_t>((i),(j-1))
		+ sobel_kernel_x[0][2]*src_ptr[(i+1)*rlen+j-1] //src.at<uint8_t>((i+1),(j-1)) //
 		+ sobel_kernel_x[1][0]*src_ptr[(i-1)*rlen+j] //src.at<uint8_t>((i-1),(j)) //
		//+ sobel_kernel_x[1][1]*src_ptr[(i)*rlen+j] //src.at<uint8_t>((i),(j))
		+ sobel_kernel_x[1][2]*src_ptr[(i+1)*rlen+j] //src.at<uint8_t>((i+1),(j)) //
		+ sobel_kernel_x[2][0]*src_ptr[(i-1)*rlen+j+1] //src.at<uint8_t>((i-1),(j+1)) //
		//+ sobel_kernel_x[2][1]*src_ptr[(i)*rlen+j+1] //src.at<uint8_t>((i),(j+1))
		+ sobel_kernel_x[2][2]*src_ptr[(i+1)*rlen+j+1] ); //src.at<uint8_t>((i+1),(j+1)) ); //

	tempy = ( sobel_kernel_y[0][0]*src_ptr[(i-1)*rlen+j-1] //src.at<uint8_t>((i-1),(j-1)) //
		+ sobel_kernel_y[0][1]*src_ptr[(i)*rlen+j-1] //src.at<uint8_t>((i),(j-1)) //
		+ sobel_kernel_y[0][2]*src_ptr[(i+1)*rlen+j-1] //src.at<uint8_t>((i+1),(j-1)) //
 	        //+ sobel_kernel_y[1][0]*src_ptr[(i-1)*rlen+j] //src.at<uint8_t>((i-1),(j)) 
		//+ sobel_kernel_y[1][1]*src_ptr[(i)*rlen+j] //src.at<uint8_t>((i),(j))
		//+ sobel_kernel_y[1][2]*src_ptr[(i+1)*rlen+j] //src.at<uint8_t>((i+1),(j)) )
	        + sobel_kernel_y[2][0]*src_ptr[(i-1)*rlen+j+1] //src.at<uint8_t>((i-1),(j+1)) //
		+ sobel_kernel_y[2][1]*src_ptr[(i)*rlen+j+1] //src.at<uint8_t>((i),(j+1)) //
		+ sobel_kernel_y[2][2]*src_ptr[(i+1)*rlen+j+1] ); //src.at<uint8_t>((i+1),(j+1)) ); //

		dst_ptr[i*rlen+j] = (uint8_t) sqrt((tempy*tempy)+(tempx*tempx));
		//dst.at<uint8_t>(i,j) = sqrt((tempy*tempy)+(tempx*tempx));
		}
	}

	return;
}

// a helper function because I get const uint8_t to const int16x8_t errors
inline int16x8_t load(const uint8_t* source, int y, int x, int len) {
	const int size = 8;
	int i;
	int16_t temp_dat[size] = {0, 0, 0, 0, 0, 0, 0, 0};

	for ( i = 0; i < size; i++ ) {
		temp_dat[i] = (int16_t) source[y*len+x+i];
	}
	
	return vld1q_s16((const int16_t*) temp_dat );
}

void sobel_neon(const Mat& src, Mat& dst)
{
	int i, j, k, l,c;
	int16_t tempx,tempy;	// 8 bits is too small, also some operations give negatives?

	const uint8_t* src_ptr = src.ptr<uint8_t>();	
	uint8_t* dst_ptr = dst.ptr<uint8_t>();
	const int rlen = src.cols;

	// evaluate 8 pixels in a group, chaining all the operations
	// do the quick NEON parts first in this strategy
	// use int16x8_t vld1q_s16 (*int16_t const * ptr) to load the src data
	// use int16x8_t vmlaq_n_s16 (int16x8_t a, int16x8_t b, int16x8_t c) to mul-acc
	// use int16x8_t vaddq_s16 (int16x8_t a, int16x8_t b) to add
	// use int16x8_t vsubq_s16 (int16x8_t a, int16x8_t b) to subtract
	// use void vst1q_s16 (int16_t * ptr, int16x8_t val) to store result
	// need vectors 8x for -2, -1, 1, 2 for each operation and initialize output 0
	// need to load source vector 8x

	const int neon_size = 8;	// should divide nicely with most inputs

	const int16_t zeros[] = {0, 0, 0, 0, 0, 0, 0, 0};
	int16_t neonx[8];
	int16_t neony[8];

	int16x8_t sdat;
	int16x8_t accx;
	int16x8_t accy;

	// calculate the first row later, first pixel after that
	for (i = 1; i < src.rows; i++) {
		for (j = 0 ; j < src.cols; j+= neon_size ) {

			accx = vld1q_s16(zeros);
			accy = vld1q_s16(zeros);

/*		tempx = ( src_ptr[(i-1)*rlen+j-1]
			- src_ptr[(i+1)*rlen+j-1]
 			+ 2*src_ptr[(i-1)*rlen+j]
			- 2*src_ptr[(i+1)*rlen+j]
			+ src_ptr[(i-1)*rlen+j+1]
			- src_ptr[(i+1)*rlen+j+1] );

		tempy = ( src_ptr[(i-1)*rlen+j-1]
			+ 2*src_ptr[(i)*rlen+j-1]
			+ src_ptr[(i+1)*rlen+j-1]
		        - src_ptr[(i-1)*rlen+j+1]
			- 2*src_ptr[(i)*rlen+j+1]
			- src_ptr[(i+1)*rlen+j+1] );	*/

		// big mess of math, I think this is correct, maybe
			//sdat = vld1q_s16((const int16_t*) src_ptr+(i-1)*rlen+j-1);
			sdat = load(src_ptr,(i-1),(j-1),rlen);
			accx = vaddq_s16(accx,sdat);
			accy = vaddq_s16(accy,sdat);
			//sdat = vld1q_s16((const int16_t*) src_ptr+(i+1)*rlen+j-1);
			sdat = load(src_ptr,(i+1),(j-1),rlen);
			accx = vsubq_s16(accx,sdat);
			accy = vaddq_s16(accy,sdat);
			//sdat = vld1q_s16((const int16_t*) src_ptr+(i-1)*rlen+j);
			sdat = load(src_ptr,(i-1),j,rlen);
			accx = vmlaq_n_s16(accx,sdat,2);
			//sdat = vld1q_s16((const int16_t*) src_ptr+(i+1)*rlen+j);
			sdat = load(src_ptr,(i+1),j,rlen);
			accx = vmlsq_n_s16(accx,sdat,2);
			//sdat = vld1q_s16((const int16_t*) src_ptr+(i)*rlen+j-1);
			sdat = load(src_ptr,i,(j-1),rlen);
			accy = vmlaq_n_s16(accy,sdat,2);
			//sdat = vld1q_s16((const int16_t*) src_ptr+(i)*rlen+j+1);
			sdat = load(src_ptr,i,(j+1),rlen);
			accy = vmlsq_n_s16(accy,sdat,2);
			//sdat = vld1q_s16((const int16_t*) src_ptr+(i-1)*rlen+j+1);
			sdat = load(src_ptr,(i-1),(j+1),rlen);
			accx = vaddq_s16(accx,sdat);
			accy = vsubq_s16(accy,sdat);
			//sdat = vld1q_s16((const int16_t*) src_ptr+(i+1)*rlen+j+1);
			sdat = load(src_ptr,(i+1),(j+1),rlen);
			accx = vsubq_s16(accx,sdat);
			accy = vsubq_s16(accy,sdat);

/*			// when I do this in NEON, something overflows and RMSE increases
			// or is there some other error?  Hmm.
			// square the results
			accx = vmulq_s16(accx,accx);
			accy = vmulq_s16(accy,accy);

			// add the two
			accx = vaddq_s16(accx,accy);
			vst1q_s16(tempx,accx);		*/

			// use void vst1q_s16 (int16_t * ptr, int16x8_t val) to store result
			//vs1q_s16 ((dst_ptr+i*rlen+j),(uint8_t*) /// ???
			vst1q_s16(neonx,accx);
			vst1q_s16(neony,accy);

			for ( c = 0; c < 8; c++ ) {
				dst_ptr[(i*rlen)+j+c] = (uint8_t) //sqrt(tempx[c]);
							sqrt((neony[c]*neony[c])
							+ (neonx[c]*neonx[c]));
			}
			//dst_ptr[i*rlen+j] = (uint8_t) sqrt((tempy*tempy)+(tempx*tempx));
		}
	}

	// why optimize this with NEON?  It's a small percent of work anyway
	i = 0;	//separate loop for the first row
	for (j = 1; j < src.cols; j++) {
		tempx = ( -src_ptr[(i+1)*rlen+j-1]
			- 2*src_ptr[(i+1)*rlen+j]
			- src_ptr[(i+1)*rlen+j+1] );

		tempy = ( 2*src_ptr[i*rlen+j-1]
			+ src_ptr[(i+1)*rlen+j-1]
			- 2*src_ptr[i*rlen+j+1]
			- src_ptr[(i+1)*rlen+j+1] );

		dst_ptr[i*rlen+j] = (uint8_t) sqrt((tempy*tempy)+(tempx*tempx));
	}


	j = 0;	// separate loop to go back to the first column, just need to go downards now
	for (i = 0; i < src.rows; i++) {
		tempx = ( 2*src_ptr[(i-1)*rlen+j]
			- 2*src_ptr[(i+1)*rlen+j]
		        + src_ptr[(i-1)*rlen+j+1]
			- src_ptr[(i+1)*rlen+j+1] );

		tempy = ( -src_ptr[(i-1)*rlen+j+1]
			- 2*src_ptr[(i)*rlen+j+1]
			- src_ptr[(i+1)*rlen+j+1] );

		dst_ptr[i*rlen+j] = (uint8_t) sqrt((tempy*tempy)+(tempx*tempx));
	}

	i = 0; // upper left pixel
	j = 0;
	tempx = ( -2*src_ptr[rlen] - src_ptr[rlen+1] );
	tempy = ( -2*src_ptr[1] - src_ptr[rlen+1] );

	dst_ptr[i*rlen+j] = (uint8_t) sqrt((tempy*tempy)+(tempx*tempx));

	return;
}


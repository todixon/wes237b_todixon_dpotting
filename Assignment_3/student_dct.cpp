//
//  student_dct.cpp
//
//  WES 237B Assignment 3
//  Torry Dixon
//  David Pottinger
//  8/5/2017
//
//  based on dct/student_dct.cpp created by Alireza on 7/6/17.
//  Copyright © 2017 Alireza. All rights reserved.

#include "main.h"


using namespace cv;


Mat LUT_w;
Mat LUT_h;


// Helper function
float sf(int in){
	if (in == 0)
		return 0.70710678118; // = 1 / sqrt(2)
	return 1.;
}

// Initialize LUT
void initDCT(int WIDTH, int HEIGHT)
{

	// TODO
	int x,y,i,j;

	// setup scale factors
	float scale_w = sqrt(2) / sqrt(WIDTH);
	float scale_h = sqrt(2) / sqrt(HEIGHT);

	LUT_w = Mat(WIDTH,WIDTH,CV_32FC1);
	LUT_h = Mat(HEIGHT,HEIGHT,CV_32FC1);

	// init LUT_w
	for(i=0; i<WIDTH; i++) {
		for(j=0; j<WIDTH; j++) {
			LUT_w.at<float>(i,j) = 0.0;
		}
	}

	// init LUT_h
	for(i=0; i<HEIGHT; i++) {
		for(j=0; j<HEIGHT; j++) {
			LUT_h.at<float>(i,j) = 0.0;
		}
	}

	for(x=0; x<HEIGHT; x++) {
        for(i=0; i<HEIGHT; i++) {
            LUT_h.at<float>(x,i) = cos(M_PI/((float)HEIGHT)*(i+1.0/2.0)*(float)x)*scale_h;
	        LUT_h.at<float>(x,i) *= sf(x);
        }
	}

    for(y=0; y<WIDTH; y++) {
        for(j=0; j<WIDTH; j++) {
            LUT_w.at<float>(y,j) = cos(M_PI/((float)WIDTH)*(j+1.0/2.0)*(float)y)*scale_w;
	        LUT_w.at<float>(y,j) *= sf(y);
        }
	}

}

// Part C - Block Matrix Multiplication
//Mat student_dct(Mat input, int blocksize) //used for data collection
Mat student_dct(Mat input)
{
	// -- Works only for WIDTH == HEIGHT
	assert(input.rows == input.cols);

	// -- Matrix multiply with OpenCV
	//Mat output = LUT_w * input * LUT_w.t();

	// TODO
	// Replace the line above by your own matrix multiplication code
	// You can use a temp matrix to store the intermediate result

	int i,j,k,jj,kk;
	//int blocksize = 192;

	const int MATRIX_SIZE = input.rows;
	
	Mat output = Mat(MATRIX_SIZE, MATRIX_SIZE, CV_32FC1);
	Mat temp = Mat(MATRIX_SIZE,MATRIX_SIZE,CV_32FC1);

	for(i=0; i<MATRIX_SIZE; i++) {
		for(j=0; j<MATRIX_SIZE; j++) {
			temp.at<float>(i,j) = 0.0;
			output.at<float>(i,j) = 0.0;
		}
	}

	float* input_ptr = input.ptr<float>();
	float* temp_ptr = temp.ptr<float>();
	float* output_ptr = output.ptr<float>();
	float* LUT_w_ptr = LUT_w.ptr<float>();

    // transpose lookup table
    //transpose = LUT_w.t();

	// temp = LUT_w*input
	for(k=0; k<MATRIX_SIZE; k+=blocksize) // LUT rows
	{
		for(j=0; j<MATRIX_SIZE; j+=blocksize) // input cols
		{
			for(i=0; i<MATRIX_SIZE; i++) // LUT cols
        	{
				for(jj=j; jj<min(j+blocksize, MATRIX_SIZE); jj++)
				{
					for(kk=k; kk<min(k+blocksize, MATRIX_SIZE); kk++)
					{
						temp_ptr[i*MATRIX_SIZE+jj] += LUT_w_ptr[i*MATRIX_SIZE+kk]*input_ptr[kk*MATRIX_SIZE+jj];
					}
				}
			}

		}
	}

	// output = temp * LUT_w.t()
	for(k=0; k<MATRIX_SIZE; k+=blocksize) // input rows
	{
		for(j=0; j<MATRIX_SIZE; j+=blocksize) // LUT_T cols
		{
			for(i=0; i<MATRIX_SIZE; i++) // input cols
        	{
				for(jj=j; jj<min(j+blocksize, MATRIX_SIZE); jj++)
				{
					for(kk=k; kk<min(k+blocksize, MATRIX_SIZE); kk++)
					{
						output_ptr[i*MATRIX_SIZE+jj] += temp_ptr[i*MATRIX_SIZE+kk]*LUT_w_ptr[jj*MATRIX_SIZE+kk];
					}
				}
			}
		}
	}
	return output;
}


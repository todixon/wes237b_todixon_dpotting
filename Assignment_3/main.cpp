//
//  main.cpp
// 
//  WES 237B Assignment 3
//  Torry Dixon
//  David Pottinger
//  8/5/2017
//
//  based on Lab2 main.cpp created by Alireza on 7/6/17.
//  Copyright © 2017 Alireza. All rights reserved.


#include "main.h"
#include "cycletime.h"

using namespace std;
using namespace cv;

int main(int argc, const char * argv[])
{
	unsigned int c_start;
	long long unsigned int cache_access = 0;
	unsigned int cache_miss = 0;
	unsigned int opencv_c, student_c;

	//int RUNS = 10; //used for data collection

	// configure register 0x0 to read cache accesses (PMUEVENT[3]) (table A.8.1 of ARM Cortex-A9 rev. r4p1, available on piazza)
	pmn_config(0x0, 0x04);
	
	// configure register 0x1 to read cache misses (PMUEVENT[4]) (table A.8.1 of ARM Cortex-A9 rev. r4p1, available on piazza)
	pmn_config(0x1, 0x03);

	init_counters(1, 0);

	cout << "WES237B HW 3" << endl;

	VideoCapture cap("input.raw");

	Mat frame, gray, dct_org, dct_student, diff_img;
	char key=0;
	float mse;
	int fps_cnt = 0;

	int WIDTH  = 64;
	int HEIGHT = 64;

	int BLOCKSIZE = WIDTH;

	// 1 argument on command line: WIDTH = HEIGHT = arg
	if(argc >= 2)
	{
		WIDTH = atoi(argv[1]);
		HEIGHT = WIDTH;
	}
	// 2 arguments on command line: WIDTH = arg1, HEIGHT = arg2
	if(argc >= 3)
	{
		HEIGHT = atoi(argv[2]);
	}
	// 3 arguments on command line: WIDTH = arg1, HEIGHT = arg2, BLOCKSIZE = arg3 - used for data collection
	if(argc >= 4) {
		BLOCKSIZE = atoi(argv[3]);
	}

	initDCT(WIDTH, HEIGHT);

	float avg_perf = 0.f;
	long long unsigned int avg_cycles = 0;
	int count = 0;


	while(key != 'q')
	//for(int i=0; i<RUNS; i++)	//used for data collection
	{
		cap >> frame;
		if(frame.empty()){ break; }

		cvtColor(frame, gray, CV_BGR2GRAY);
		resize(gray, gray, Size(WIDTH, HEIGHT));
		gray.convertTo(gray, CV_32FC1);

		init_counters(1, 0);

		// OpenCV DCT
		c_start = get_cyclecount();
		dct(gray, dct_org);
		opencv_c = get_cyclecount() - c_start;

		// Your DCT
		// start counting
		c_start = get_cyclecount();
		cache_access = pmn_read(0x0);
		cache_miss = pmn_read(0x1);

		dct_student = student_dct(gray);
		// dct_student = student_dct(gray,BLOCKSIZE);  //used for data collection

		// stop counting
		student_c = get_cyclecount() - c_start;
		cache_miss = pmn_read(0x1) - cache_miss;
		cache_access = pmn_read(0x0) - cache_access;
		float cachehit_ratio = float(cache_access-cache_miss)/cache_access;

		gray.convertTo(gray, CV_8UC1);

		absdiff(dct_org, dct_student, diff_img); 

		/* calculating RMSE */
		diff_img = diff_img.mul(diff_img);
		Scalar se = sum(diff_img);
		mse = se[0]/((float)HEIGHT*WIDTH);

		count++;

		float perf = 100.f*(float)opencv_c/(float)student_c;
		avg_perf += perf;
		avg_cycles += student_c;
		printf("Perf: %.3f%% (avg: %.3f%%), Cycles: %d (avg: %llu), RMSE: %.4f\n",
				perf, avg_perf/count,
				student_c, avg_cycles/count,
				sqrt(mse);
		printf("cache misses: %d\t cache accesses: %llu\t cache hit ratio: %.4f\n",
				cache_miss,
				cache_access,
				cachehit_ratio);

		//printf("%d\t %d\t %d\t %llu\t %.4f\n", WIDTH, BLOCKSIZE, cache_miss, cache_access-cache_miss, cachehit_ratio);  // used for data collection


		Mat inverse;
		idct(dct_student, inverse);
		inverse.convertTo(inverse, CV_8U);

		imshow("Original", gray);
		imshow("IDCT Output", inverse);
		moveWindow("IDCT Output", 500, 0);
		key = waitKey(10);
	}

	return 0;
}





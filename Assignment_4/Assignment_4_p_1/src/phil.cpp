//
//  phil.cpp
//
//  WES 237B Assignment 4
//  Torry Dixon
//  David Pottinger
//  8/20/2017
//
//  based on lab4/LED.cpp created by Alireza on 7/6/17.
//  Copyright © 2017 Alireza. All rights reserved.

#include "phil.h"


using namespace std;

// semaphore init
sem_t mutex;
sem_t S[N];

int state[N];
int phil_num[N]={0,1,2,3};
int min_eat, max_eat, min_think, max_think;
int eat_time, think_time;

int main(int argc, char *argv[])
{
    int i;

	// get min and max eat times
    min_eat = atoi(argv[1])/1000;
    max_eat = atoi(argv[2])/1000;
    eat_time = rand() % (max_eat+1 - min_eat) + min_eat;
    
	// get min and max think times
    min_think = atoi(argv[3])/1000;
    max_think = atoi(argv[4])/1000;
    think_time = rand() % (max_think+1 - min_think) + min_think;

	// setup mutex and semaphores
    pthread_t thread_id[N];
    sem_init(&mutex,0,1);
    for(i=0;i<N;i++) {
        sem_init(&S[i],0,0);
	}

	// create threads
    for(i=0;i<N;i++) {
        pthread_create(&thread_id[i],NULL,philospher,&phil_num[i]);
        //printf("Philosopher %d is thinking\n",i+1);
		// set up LEDs		
		init_led(i+4);
		led_state(i+4,OFF);
    }
	
	// waits for threads to complete
    for(i=0;i<N;i++) {
        pthread_join(thread_id[i],NULL);
    }
}
 
// starts the whole think/eat/wait cycle
void *philospher(void *num)
{
    while(1)
    {
        int *i = (int *)num;
        sleep(1);
        take_fork(*i);
        sleep(0);
        put_fork(*i);
    }
}
 
// start waiting
void take_fork(int ph_num)
{
    sem_wait(&mutex);
    state[ph_num] = WAITING;
    //printf("Philosopher %d is waiting\n",ph_num+1);
    test(ph_num);

	// turn LED off
	led_state(ph_num+4,OFF);
    sem_post(&mutex);
    sem_wait(&S[ph_num]);
    sleep(1);
}
 
// ensures philosopher has both forks before eating
void test(int ph_num) {
    if (state[ph_num] == WAITING && state[LEFT] != EATING && state[RIGHT] != EATING)
    {
		state[ph_num] = EATING;
		sleep(1);
		//printf("Philosopher %d takes fork %d and %d\n",ph_num+1,LEFT+1,ph_num+1);
		//printf("Philosopher %d is Eating\n",ph_num+1);
		
		// blink LED while eating
		blink_led(ph_num+4,eat_time);
		sleep(eat_time);
		sem_post(&S[ph_num]);
    }
}
 
// puts both forks down and thinks
void put_fork(int ph_num) {
    sem_wait(&mutex);
    state[ph_num] = THINKING;
    sleep(think_time);
    //printf("Philosopher %d putting fork %d and %d down\n",ph_num+1,LEFT+1,ph_num+1);
    //printf("Philosopher %d is thinking\n",ph_num+1);
	on_led(ph_num+4,think_time);
    test(LEFT);
    test(RIGHT);
    sem_post(&mutex);
}

// sets up Zedboard LEDs to show thinking/eating/waiting
void init_led(int led) {
	ostringstream led_s;
	led_s << (led+57);
	string name = "/sys/class/gpio/gpio";
	name.append(led_s.str());
	if(access(name.c_str(), F_OK) == -1) {
		string str1 = "echo ";
		str1.append(led_s.str());
		str1.append(" > /sys/class/gpio/export");
		system(str1.c_str());

		string str2 = "echo out > /sys/class/gpio/gpio";
		str2.append(led_s.str());
		str2.append("/direction");
		system(str2.c_str());
    } else {
		cout << "led_" << led_s.str() << " is already initialized." << endl;
	}
}

// turns LEDs on or off
void led_state(int led, int v) {
	ostringstream led_s;
	led_s << (led+57);
	ostringstream v_s;
	v_s << (v);
	string str = "echo ";
	str.append(v_s.str());
	str.append(" > /sys/class/gpio/gpio");
	str.append(led_s.str());
	str.append("/value");
	system(str.c_str());
}

// blink LEDs for specified time
void blink_led(int led, int blinktime) {
	for(int i=0; i<blinktime*10; i++) {	
		if(i % 2 == 0) {
			led_state(led, ON);
		} else {
			led_state(led, OFF);
		}
		usleep(50000);
	}
}

// turns on LED for specified time
void on_led(int led, int ontime) {
	led_state(led, ON);
	sleep(ontime);
	led_state(led, OFF);
}

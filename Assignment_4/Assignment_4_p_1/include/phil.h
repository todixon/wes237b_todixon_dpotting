//  phil.h
//
//  WES 237B Assignment 4
//  Torry Dixon
//  David Pottinger
//  8/20/2017
//
//  based on lab2/main.h created by Alireza on 7/6/17.
//  Copyright © 2017 Alireza. All rights reserved.

#ifndef phil_h
#define phil_h

#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>
#include <fcntl.h>

#define N 4
#define THINKING 0
#define WAITING 1
#define EATING 2
#define LEFT (ph_num+3)%N
#define RIGHT (ph_num+1)%N
#define ON 1
#define OFF 0

void * philospher(void *num);
void take_fork(int);
void put_fork(int);
void test(int);
void init_led(int led);
void led_state(int led, int v);
void blink_led(int led, int blinktime);
void on_led(int led, int ontime);

#endif /* phil_h */

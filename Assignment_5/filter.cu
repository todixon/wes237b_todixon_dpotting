#include "filter.h"
#include "cuda_timer.h"

#include <iostream>

using namespace std;


__global__
void kernel_sobel_filter(const uchar * input, uchar * output, const uint height, const uint width)
{
	int x,y;

	const int sobel_x[3][3] = {
		{-1, 0, 1},
		{-2, 0, 2},
		{-1, 0, 1}
	};
	const int sobel_y[3][3]  = {
		{-1, -2, -1},
		{0,   0,  0},
		{1,   2,  1}
	};

	y = blockDim.y*blockIdx.y + threadIdx.y+1;
	if ( y > (height-1) )
		return;

	x = blockDim.x*blockIdx.x + threadIdx.x+1;
	if ( x > (width-1) )
		return;

	const int pixel_x = (int) (
		(sobel_x[0][0] * input[x-1 + (y-1) * width]) + 
		(sobel_x[0][1] * input[x   + (y-1) * width]) + 
		(sobel_x[0][2] * input[x+1 + (y-1) * width]) +
		(sobel_x[1][0] * input[x-1 + (y  ) * width]) + 
		(sobel_x[1][1] * input[x   + (y  ) * width]) + 
		(sobel_x[1][2] * input[x+1 + (y  ) * width]) +
		(sobel_x[2][0] * input[x-1 + (y+1) * width]) + 
		(sobel_x[2][1] * input[x   + (y+1) * width]) + 
		(sobel_x[2][2] * input[x+1 + (y+1) * width])
		);
	const int pixel_y = (int) (
		(sobel_y[0][0] * input[x-1 + (y-1) * width]) + 
		(sobel_y[0][1] * input[x   + (y-1) * width]) + 
		(sobel_y[0][2] * input[x+1 + (y-1) * width]) +
		(sobel_y[1][0] * input[x-1 + (y  ) * width]) + 
		(sobel_y[1][1] * input[x   + (y  ) * width]) + 
		(sobel_y[1][2] * input[x+1 + (y  ) * width]) +
		(sobel_y[2][0] * input[x-1 + (y+1) * width]) + 
		(sobel_y[2][1] * input[x   + (y+1) * width]) + 
		(sobel_y[2][2] * input[x+1 + (y+1) * width])
		);

	float magnitude = sqrt((float)(pixel_x * pixel_x + pixel_y * pixel_y));

	if (magnitude < 0){ magnitude = 0; }
	if (magnitude > 255){ magnitude = 255; }

	output[x + y * width] = magnitude;

}

inline int divup(int a, int b)
{
	if (a % b)
		return a / b + 1;
	else
		return a / b;
}

/**
 * Wrapper for calling the kernel.
 */
void sobel_filter_gpu(const uchar * input, uchar * output, const uint height, const uint width)
{
	const int size = height * width * sizeof(uchar);

	CudaSynchronizedTimer timer;


	// Launch the kernel
	// fix the block size to avoid a crash with (block_x * block_y) > 1024
	const int block_x = 32;
	const int block_y = 32;

	dim3 grid(divup((width),block_x), divup((height),block_y), 1);
	dim3 block(block_x, block_y, 1);

	timer.start();
	kernel_sobel_filter<<<grid, block>>>(input, output, height, width);
	timer.stop();

	cudaDeviceSynchronize();

	float time_kernel = timer.getElapsed();
}


void sobel_filter_cpu(const uchar * input, uchar * output, const uint height, const uint width)
{
	const int sobel_x[3][3] = {
		{-1, 0, 1},
		{-2, 0, 2},
		{-1, 0, 1}
	};
	const int sobel_y[3][3]  = {
		{-1, -2, -1},
		{0,   0,  0},
		{1,   2,  1}
	};

	for (uint y = 1; y < height - 1; ++y)
	{
		for (uint x = 1; x < width - 1; ++x)
		{

			const int pixel_x = (int) (
					(sobel_x[0][0] * input[x-1 + (y-1) * width]) + 
					(sobel_x[0][1] * input[x   + (y-1) * width]) + 
					(sobel_x[0][2] * input[x+1 + (y-1) * width]) +
					(sobel_x[1][0] * input[x-1 + (y  ) * width]) + 
					(sobel_x[1][1] * input[x   + (y  ) * width]) + 
					(sobel_x[1][2] * input[x+1 + (y  ) * width]) +
					(sobel_x[2][0] * input[x-1 + (y+1) * width]) + 
					(sobel_x[2][1] * input[x   + (y+1) * width]) + 
					(sobel_x[2][2] * input[x+1 + (y+1) * width])
					);
			const int pixel_y = (int) (
					(sobel_y[0][0] * input[x-1 + (y-1) * width]) + 
					(sobel_y[0][1] * input[x   + (y-1) * width]) + 
					(sobel_y[0][2] * input[x+1 + (y-1) * width]) +
					(sobel_y[1][0] * input[x-1 + (y  ) * width]) + 
					(sobel_y[1][1] * input[x   + (y  ) * width]) + 
					(sobel_y[1][2] * input[x+1 + (y  ) * width]) +
					(sobel_y[2][0] * input[x-1 + (y+1) * width]) + 
					(sobel_y[2][1] * input[x   + (y+1) * width]) + 
					(sobel_y[2][2] * input[x+1 + (y+1) * width])
					);

			float magnitude = sqrt((float)(pixel_x * pixel_x + pixel_y * pixel_y));

			if (magnitude < 0){ magnitude = 0; }
			if (magnitude > 255){ magnitude = 255; }

			output[x + y * width] = magnitude;
		}
	}
}




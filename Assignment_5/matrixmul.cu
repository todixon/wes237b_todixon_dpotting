#include <stdio.h>
#include <stdlib.h>

#include "matrixmul.h"
#include "cuda_error.h"

#define BLOCK_SIZE 16

__global__ void block_mm_kernel(const float* A, const float* B, float* output, int M, int N) 
{
	// WES 237B Assignment 5
	//
	// Torry Dixon
	// David Pottinger
	//
	// 8/25/17
	//
	
	// block indexes
	int bx = blockIdx.x;
	int by = blockIdx.y;
	
	// thread indexes	
	int tx = threadIdx.x;
	int ty = threadIdx.y;

	// A indexes
	int aBegin = (M * BLOCK_SIZE) * by;
	int aEnd = aBegin + M - 1;
	int aStep = BLOCK_SIZE;
	
	// B indexes
	int bBegin = BLOCK_SIZE * bx;
	int bStep = BLOCK_SIZE * N;

	// output element per subthread
	float cSub = 0;
	
	// loop over the block to calculate intermediate results
	for(int a = aBegin, b = bBegin; a <= aEnd; a += aStep, b += bStep) {

		// declares shared memory for the blocks
		__shared__ float As[BLOCK_SIZE][BLOCK_SIZE];
		__shared__ float Bs[BLOCK_SIZE][BLOCK_SIZE];

		// loads submatrices from global memory into shared memory
		// loads one element of each matrix per thread
		As[ty][tx] = A[a + (M * ty) + tx];
		Bs[ty][tx] = B[b + (N * ty) + tx];

		// syncs to make sure matrices are loaded
		__syncthreads();

		// multiply matrices with each thread computing one element of the submatrix
		for(int k = 0; k < BLOCK_SIZE; ++k) {
			cSub += As[ty][k] * Bs[k][tx];

			// sync to ensure the calculation is complete before moving to the next iteration
			__syncthreads();
		}

		// write each submatrix to global memory, with each thread writing one element
		int c = (N * BLOCK_SIZE * by) + (BLOCK_SIZE * bx);
		output[c + (N * ty) + tx] = cSub;
	}
 }


inline int divup(int a, int b)
{
	if (a % b)
		return a / b + 1;
	else
		return a / b;
}


float run_mm_gpu(const float* A, const float* B, float* C, int M, int N)
{
	// Profiling
	float time_gpu;

	cudaEvent_t start, stop;
	CudaSafeCall(cudaEventCreate(&start));
	CudaSafeCall(cudaEventCreate(&stop));

	CudaSafeCall(cudaEventRecord(start, 0));
	
	dim3 dimGrid(divup(N, BLOCK_SIZE), divup(N, BLOCK_SIZE));
	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);

	// Launch kernel 
	block_mm_kernel<<<dimGrid, dimBlock>>>(A, B, C, M, N);

	CudaCheckError();

	CudaSafeCall(cudaThreadSynchronize());

	CudaSafeCall(cudaEventRecord(stop, 0));
	CudaSafeCall(cudaEventSynchronize(stop));
	
	CudaSafeCall(cudaEventElapsedTime(&time_gpu, start, stop));

	// Free Memory
	CudaSafeCall(cudaEventDestroy(start));
	CudaSafeCall(cudaEventDestroy(stop));

	return time_gpu;
}



# WES 237B
# HW1 Part 1.4
# Torry Dixon
# David Pottinger

# convert.py

import sys
import struct

with open(fileName, mode='rb') as file:
	fileData = file.read()

struct.unpack("L", fileData)

fileDataText = fileData.decode("ascii")

output_file = open(fileName+"output", "w")
output_file.write(fileDataText)
output_file.close()
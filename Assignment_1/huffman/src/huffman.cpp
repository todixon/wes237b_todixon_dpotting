#include "huffman.h"
#include <functional>
#include <queue>
#include <vector>
#include <iostream>
#include <string>
#include <cstring>
#include <sstream>
#include <stdlib.h>
#include <iterator>
#include <cstdlib>

// test code bypassing file input just to check it out
//char arr[] = {'a', 'b', 'c', 'd', 'e', 'f'};
//char freq[] = {5, 9, 12, 13, 16, 45};
//int size = sizeof(arr) / sizeof(arr[0]);

// might as well use all the ascii characters in a big array
#define MAX_ASCII_CHAR	128 	// this is actually DEL but eh
char arr[128];
long int freq[128];
std::string scod[128];		// save leading zeros, number representations don't do this

// making a big tree, need some nodes
struct node {
	char data;		//the node's character
	unsigned freq;		//number of occurances of node character
	node *left, *right;	//many nodes have subnodes

	// now a constructor
	node(char data, unsigned freq) {
		left = right = NULL;
		this->data = data;
		this->freq = freq;
	}
};

struct compare {
	// evaluate the size (frequency count) of the subnodes
	bool operator()(node* l, node* r) {
		return(l->freq > r->freq);
	}
};

void recursivePrint(struct node* root, std::string str) {
	// handle recursion cases with no actual endpoints
	if (!root)
		return;

	// only print the nodes of actual encoded characters
	if (root->data != '$') {
		scod[root->data] = str;
	}

	recursivePrint(root->left, str + "0");
	recursivePrint(root->right, str + "1");
}

/**
 * TODO Complete this function
 **/
int huffman_encode(		const unsigned char *bufin,
				unsigned int bufinlen,
				unsigned char **pbufout,
				unsigned int *pbufoutlen	)
{
	int i;

	// more for decode, make this lookup part work
	for (i = 0; i < MAX_ASCII_CHAR; i++) {
		arr[i] = char(i);	// typecast to character
		freq[i] = 0;		// initialize as zero
	}

	// first we need to find the characters and the frequency of each
	for (i = 0; i < bufinlen; i++) {
		freq[bufin[i]]++;	// count each character
	}

	struct node *left, *right, *top;

	std::priority_queue<node*, std::vector<node*>, compare> minHeap;
	for (i = 0; i < MAX_ASCII_CHAR; ++i) {
		if ( freq[i] > 0 )	// don't encode characters that aren't used
			minHeap.push(new node(arr[i], freq[i]));
	}

	// minHeap contains starter array of characters, they're popped out until empty
	while (minHeap.size() != 1) {
		left = minHeap.top();
		minHeap.pop();

		right = minHeap.top();
		minHeap.pop();

		// create dummy nodes combining existing small numbers in the tree
		top = new node('$', left->freq + right->freq);
		top->left = left;
		top->right = right;
		minHeap.push(top);
	}

	recursivePrint(minHeap.top(), "");

	// this uses up lots of memory but it's fast
	// convert everything into a long string of 0's and 1's
	// we need to know how long this will be for the legend
	std::stringstream long_convert;

	for ( i = 0; i < bufinlen; i++) {
		long_convert << scod[bufin[i]];
	}

	std::string long_code = long_convert.str();

	// in buffer construct a legend to pass along for decoding later
	std::stringstream buffer;

	// first we put in buffer the length of bits that we use to encode
	// this becomes a problem later when we make try to decode bytes
	// newline \n will be our delimiter
	buffer << long_code.length() << "\n";

	// now add the actual strings for the legend
	for ( i = 0; i < MAX_ASCII_CHAR; i++) {
		if ( freq[i] > 0) {
			buffer << arr[i] << ": " << scod[i] << "\n";
		}
	}

	std::string legend = buffer.str();
	legend.append("\n\n\n");	// separate decode legend from data
	
	// then convert that long string in "code" into binary 8 bits at a time
	unsigned int short_length = (long_code.length()/8) + 1 + strlen(legend.c_str());
	unsigned char* short_code = (unsigned char *) malloc(short_length);

	memcpy(short_code,legend.c_str(),strlen(legend.c_str())+1);

	// let's just make this simple and compatable.  Endianness?  As long as I preserve it, left to right
	unsigned int j = 0;
	for ( i = legend.length(); i < short_length; i++ ) {
		unsigned char temp = 0;
		unsigned char byte_loc;
		for (byte_loc = 0x80; byte_loc > 0; ) {
			if ( j < long_code.length() )
				if (long_code[j] == '1')
					temp += byte_loc;
			byte_loc = byte_loc >> 1;
			// loop will end when byte_loc goes to 0
			j++;
		}
		short_code[i] = temp;
		if ( j > long_code.length() )
			break;
	}

	// this part is annoying and took some time just to convert, why unsigned char arrays?
	*pbufout = short_code;
	*pbufoutlen = short_length;

	return 0;
}


/**
 * TODO Complete this function
 **/
int huffman_decode(		const unsigned char *bufin,
				unsigned int bufinlen,
				unsigned char **pbufout,
				unsigned int *pbufoutlen	)
{
	// first separate the binary part of the file from the decoding legend
	unsigned int i, j;
	unsigned int dataloc;
	int bitlength;
	unsigned char temp;
	std::string mediate[MAX_ASCII_CHAR];	// intermediate table
	std::string lookup[MAX_ASCII_CHAR];	// lookup for each used character
	std::string s;

	std::stringstream legend;
	std::stringstream databits;

	for ( i = 0; i < bufinlen; i++) {
		legend << (unsigned char) bufin[i]; // these are just ascii characters
		// look for three line \n in a row
		if ((bufin[i] == '\n') && (bufin[i+1] == '\n') && (bufin[i+2] == '\n')) {
			dataloc = i+4;
			i = bufinlen;
			break;
		}
	}

	//std::cout << legend.str();
	//std::cout << databits.str();

	// populate lookup strings based on legend from code.txt
	// we expect the length of bits, newline, legend with newlines
	s = legend.str();

	j = 0;
	int found = 0;

	found = s.find("\n",j);
	bitlength = std::atoi(s.substr(0,found).c_str());
	j = found+1;
	for ( i = 0; i < MAX_ASCII_CHAR; i++) {
		found = s.find("\n",j+1);	// add one for the case there's a newline immeiately after as lookup
		if ( found == std::string::npos )
			break;

		mediate[i] = s.substr(j,found-j);
		j = found+1;
	}

	// take first mediate character and use it to insert into lookup array in that position
	for ( j = 0; j < i; j++) {
		temp = mediate[j].at(0);
		lookup[temp] = mediate[j].substr(3);
		//std::cout << temp << " : " << lookup[temp] << "\n";
	}

	// convert data section into string of 1's and 0's
	i = 0;
	while ( dataloc < bufinlen ) {
		temp = bufin[dataloc];
		for (j = 0x80; j > 0; ) {
			if ( (temp & j) == 0) 	// bit is clear
				databits << '0';
			else			// bit is set
				databits << '1';
			j = j >> 1;	// shift right one

			i++;
			if ( i > bitlength ) {
				// we've retreived all the original bits, leftover is due to byte boundary
				// I actually had an error related to this
				dataloc = bufinlen;
				break;
			}
		}
		dataloc++;
	}

	// use i and j to select a substring, compare to various lookups to build the decoded string
	std::string cmp,out;
	s = databits.str();
	j = 0;
	unsigned char c;
	for ( i = 1; i < s.length()+1; i++) {
		cmp = s.substr(j,i-j);
		//std::cout << cmp << "\n";
		// check each of these every time we add a new character
		for (c = 0; c < MAX_ASCII_CHAR; c++) {
			if ( lookup[c] == cmp ) {
				out += c;
				//std::cout << "\n\t Think this character is \"" << c << "\"\n";
				//while(1);
				j = i; // stop considering the used characters
				break;	// leave the inner loop checking for decode
			}
		}
	}
	//std::cout << cmp << "\n";
	// newline lookup doesn't get saved
	//out += '\n';

	unsigned long int decode_len = strlen(out.c_str())+1;
	unsigned char* decode_res = (unsigned char *) malloc(decode_len);

	memcpy(decode_res,out.c_str(),decode_len);

	*pbufout = decode_res;
	*pbufoutlen = decode_len;

  	return 0;
}

